@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")

package react.materialui

import react.RClass

@JsModule("@material-ui/core/IconButton/IconButton")
external val IconButtonImport: dynamic

external interface IconButtonProps : ButtonProps {
}

var IconButton: RClass<ButtonProps> = IconButtonImport.default
